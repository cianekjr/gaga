import Vue from 'vue'
import VueRouter from 'vue-router'
import { axiosInstance } from './axios'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: () => import('../views/Dashboard.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue'),
    meta: { requiresAuth: false }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, _, next) => {
  const response = await axiosInstance.get('auth', { withCredentials: true })
  const hasAccess = response.data.access

  if (hasAccess) {
    if (to.path === '/login') {
      next('/')
    } else {
      next()
    }
  } else {
    if (to.meta.requiresAuth) {
      next('/login')
    } else {
      next()
    }
  }
})

export default router
