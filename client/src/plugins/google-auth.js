import Vue from 'vue'
import GAuth from 'vue-google-oauth2'

const gauthOption = {
  clientId: '424846620102-ro101rqb04cv222pmh8gj5vekph5i4po.apps.googleusercontent.com',
  prompt: 'consent',
  scope: 'email profile',
  fetch_basic_profile: false
}
Vue.use(GAuth, gauthOption)
