import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

export const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  withCredentials: true
})

Vue.use(VueAxios, axiosInstance)
