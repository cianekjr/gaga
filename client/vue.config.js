module.exports = {
  devServer: {
    port: 8080,
    disableHostCheck: true,
    host: '0.0.0.0'
  }
}
