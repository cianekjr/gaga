package structs

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	Email string `json:"username"`
	jwt.StandardClaims
}
