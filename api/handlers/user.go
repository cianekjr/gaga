package handlers

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"os"
)

func User(c *gin.Context) {
	var jwtKey = []byte(os.Getenv("JWT_SECRET"))

	authToken, _ := c.Cookie("auth_token.gatekeeper")

	token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		} else {
			return jwtKey, nil
		}
	})

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}

	//if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
	//	fmt.Println(claims["foo"], claims["nbf"])
	//} else {
	//	fmt.Println(err)
	//}

	isTokenValid := false

	if token.Valid {
		isTokenValid = true
	}

	c.JSON(200, gin.H{
		"access":  isTokenValid,
	})
}
