package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"gatekeeper/server/config"
	"gatekeeper/server/structs"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"time"

	"io/ioutil"
	"net/http"
	"os"
)

func exchangeAuthCodeForToken (c *gin.Context) *oauth2.Token {
	var authData structs.Auth
	conf := config.GoogleAuth()
	ctx := context.Background()

	if err := c.ShouldBindJSON(&authData); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return nil
	}

	token, err := conf.Exchange(ctx, authData.Code)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return nil
	}

	return token
}

func getUserProfile (c *gin.Context, token *oauth2.Token) structs.User {
	ctx := context.Background()
	conf := config.GoogleAuth()
	var user structs.User

	googleClient := conf.Client(ctx, token)

	clientResponse, err := googleClient.Get(os.Getenv("GOOGLE_USER_API"))

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return user
	}

	defer clientResponse.Body.Close()
	userData, err := ioutil.ReadAll(clientResponse.Body)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return user
	}

	err = json.Unmarshal(userData, &user)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return user
	}

	return user
}

func generateJwtToken(userProfile structs.User) string {
	jwtKey := []byte(os.Getenv("JWT_SECRET"))
	expirationTime := time.Now().Add(24 * 60 * time.Minute)

	claims := &structs.Claims{
		Email: userProfile.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	generatedJwt := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := generatedJwt.SignedString(jwtKey)

	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return ""
	}

	return tokenString
}

func checkIfUserValid(userProfile structs.User ) bool {
	//Check in database if user's email domain is daftcode.pl/daftcode.com/datco.de
	// userAllowed := userProfile.EmailVerified == true
	userAllowed := userProfile.Email == "cianekjr@gmail.com"

	return userAllowed
}

func AuthLogin (c *gin.Context) {
	token := exchangeAuthCodeForToken(c)

	userProfile := getUserProfile(c, token)

	userAllowed := checkIfUserValid(userProfile)

	if !userAllowed {
		return
	}

	tokenString := generateJwtToken(userProfile)

	c.SetCookie("auth_token.gatekeeper", tokenString, 24*60*60 , "/", "", true, false)

	c.JSON(200, gin.H{
		"user": userProfile,
	})
}
