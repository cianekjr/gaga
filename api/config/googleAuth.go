package config

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"os"
)

func GoogleAuth() *oauth2.Config {
	conf := &oauth2.Config{
		ClientID:     os.Getenv("CLIENT_ID"),
		ClientSecret: os.Getenv("CLIENT_SECRET"),
		RedirectURL:  "postmessage",
		Scopes:       []string{"email", "profile"},
		Endpoint:     google.Endpoint,
	}
	return conf
}
